#!/bin/bash

NODES=$(wc -l < $1)
for ((i=1; i<=$NODES; i++))
do 
  node=`awk 'NR=='"$i"'{print;exit}' $1`
  echo $node

  cmd_yum_update='sudo yum -y update'
  echo $cmd_yum_update
  ssh $node $cmd_yum_update

  cmd_preq_collectd='sudo yum -y install libcurl libcurl-devel rddtool rrdtool-devel rrdtool-prel libgcrypt-devel gcc make gcc-c++'
  echo $cmd_preq_collectd
  ssh $node $cmd_preq_collectd

  cmd_get_collectd='sudo wget http://collectd.org/files/collectd-5.5.0.tar.gz'
  echo $cmd_get_collectd
  ssh $node $cmd_get_collectd

  cmd_install_collectd='sudo tar zxf collectd-5.5.0.tar.gz; cd collectd-5.5.0; ./configure; make; sudo make install; sudo cp contrib/redhat/init.d-collectd /etc/init.d/collectd; sudo chmod 755 /etc/init.d/collectd'
  echo $cmd_install_collectd
  ssh $node $cmd_install_collectd

  cmd_start_collectd='sudo service collectd start'
  echo $cmd_start_collectd
  ssh $node $cmd_start_collectd

  scp collectd.conf ${node}:~/
  cmd_config_collectd='sudo cp ~/collectd.conf /etc/collectd/'
  echo $cmd_config_collectd
  ssh $node $cmd_config_collectd

  scp apache2-graphite.conf 


  if [ $i = 1 ]; then
    echo "Install graphite on the head node"
    cmd_preq_graphite='yum -y install httpd git pycairo mod_wsgi epel-release python-pip python-devel blas-devel lapack-devel libffi-devel'
    echo $cmd_preq_graphite
    ssh $node $cmd_preq_graphite

    ssh $node 'sudo cd /usr/local/src'
    ssh $node 'sudo git clone https://github.com/graphite-project/graphite-web.git'
    ssh $node 'sudo git clone https://github.com/graphite-project/carbon.git'

    ssh $node 'sudo pip install -r /usr/local/src/graphite-web/requirements.txt'

    ssh $node 'sudo cd /usr/local/src/carbon/'
    ssh $node 'sudo python setup.py install'

    ssh $node 'sudo cd /usr/local/src/graphite-web/'
    ssh $node 'sudo python setup.py install'

    ssh $node 'sudo cp /opt/graphite/conf/carbon.conf.example /opt/graphite/conf/carbon.conf'
    ssh $node 'sudo cp /opt/graphite/conf/storage-schemas.conf.example /opt/graphite/conf/storage-schemas.conf'
    ssh $node 'sudo cp /opt/graphite/conf/storage-aggregation.conf.example /opt/graphite/conf/storage-aggregation.conf'
    ssh $node 'sudo cp /opt/graphite/conf/relay-rules.conf.example /opt/graphite/conf/relay-rules.conf'
    ssh $node 'sudo cp /opt/graphite/webapp/graphite/local_settings.py.example /opt/graphite/webapp/graphite/local_settings.py'
    ssh $node 'sudo cp /opt/graphite/conf/graphite.wsgi.example /opt/graphite/conf/graphite.wsgi'
    ssh $node 'sudo cp /opt/graphite/examples/example-graphite-vhost.conf /etc/httpd/conf.d/graphite.conf'
 
    ssh $node 'sudo cp /usr/local/src/carbon/distro/redhat/init.d/carbon-* /etc/init.d/'
    ssh $node 'sudo chmod +x /etc/init.d/carbon-*'
  fi

done 

