#!/bin/bash
currentNode=`hostname -f`

while :
do
  ps aux | grep ^$1 | awk "{print \$5}" | paste -sd+ | bc >> ${currentNode}.${1}.mem  
  sleep 5
done
