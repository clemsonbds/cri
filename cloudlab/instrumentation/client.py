# The client controls signal to the log servers on the individual nodes
import sys
import subprocess
import time
import socket
# getting list of hosts. Assuming 192.168.1.1 is for the master node, which responsible for signaling 
# the start of the instrumentation process on all nodes and the end of the instrumentation process on
# all nodes. 
nodeList = [line.rstrip('\n') for line in open('machinefiles')]

# for each nodes, start instrumentation functions (memory, bandwidth, ...) 
for line in nodeList:
  print(line)
  clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  clientsocket.connect((line,11111))
  clientsocket.send('start')
  clientsocket.close()

# execute main program
print("wait some time")
time.sleep(20)

# for each nodes, stop instrumentation
for line in nodeList:
  clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  clientsocket.connect((line,11111))
  clientsocket.send('stop')
  clientsocket.close()

# for each node, aggregate logs to 192.168.1.1
