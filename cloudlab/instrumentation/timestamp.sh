#!/bin/bash
currentNode=`hostname -f`

timestamp() {
  date +%s
}

while :
do
  currentTime=$(timestamp)
  echo "$currentTime" >> ${currentNode}.time
  sleep 5
done
