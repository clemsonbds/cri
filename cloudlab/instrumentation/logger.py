import sys
import subprocess
import time
import socket

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind(('localhost',11111))
serversocket.listen(1)
taskList = [line.rstrip('\n') for line in open('taskfiles')]
while True:
  connection, address = serversocket.accept()
  buf = connection.recv(64)
  print buf
  if (buf == 'start'):
    print('Start collection logs')
#    time_output = subprocess.Popen(["/home/lngo/git/cri/cloudlab/instrumentation/time.sh", oneTask, "logfiles"], stdout=subprocess.PIPE)
    processList = [subprocess.Popen(["/home/lngo/git/cri/cloudlab/instrumentation/memory.sh", oneTask, "logfiles"], stdout=subprocess.PIPE) for oneTask in taskList]
    for oneProc in processList:
      print(oneProc.pid)
#    for oneTask in taskList:
#      memory_output = subprocess.Popen(["/home/lngo/git/cri/cloudlab/instrumentation/memory.sh", oneTask, "logfiles"], stdout=subprocess.PIPE)
#      print(memory_output.pid)
  else:
    print('Stop collection logs')
#    memory_output.kill()
    for oneProc in processList:
      oneProc.kill()
    break

# for each node, aggregate logs to 192.168.1.1
