#!/bin/bash
sudo apt-get -y update

#sudo sh -c "echo 'sudo modprobe ipmi_msghandler' >> /etc/modules"
#sudo sh -c "echo 'sudo modprobe ipmi_devintf' >> /etc/modules"
#sudo sh -c "echo 'sudo modprobe ipmi_si' >> /etc/modules"
#sudo sh -c "echo 'sudo modprobe i2c-dev' >> /etc/modules"
#sudo sh -c "echo 'sudo modprobe i2c-i801' >> /etc/modules"
#sudo sh -c "echo 'sudo modprobe cpuid' >> /etc/modules"

#sudo apt-get -y install ipmitool
#sudo apt-get -y install module-assistant
#sudo module-assistant update
#sudo apt-get -y install lm-sensors
#sudo yes "" | sudo sensors-detect

# setup repository for Hortonworks
sudo wget http://archive.cloudera.com/cdh5/ubuntu/trusty/amd64/cdh/archive.key -O archive.key
sudo apt-key add archive.key
#sudo wget http://archive.cloudera.com/cdh5/ubuntu/trusty/amd64/cdh/cloudera.list -O /etc/apt/sources.list.d/cloudera.list
sudo sh -c "echo 'deb [arch=amd64] http://archive.cloudera.com/cdh5/ubuntu/trusty/amd64/cdh trusty-cdh5.3.3 contrib' >> /etc/apt/sources.list.d/cloudera.list"
sudo sh -c "echo 'deb-src http://archive.cloudera.com/cdh5/ubuntu/precise/amd64/cdh trusty-cdh5.3.3 contrib' >> /etc/apt/sources.list.d/cloudera.list"
sudo cp /cri/cloudlab/cloudera/preferences.d/cloudera.pref /etc/apt/preferences.d/cloudera.pref
sudo apt-get update

# set up Java
sudo apt-get -y install openjdk-7-jdk
sudo sh -c "echo 'export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64' >> /etc/bash.bashrc"
sudo sh -c "echo 'export PATH=\$PATH:\$JAVA_HOME/bin' >> /etc/bash.bashrc"
sudo sh -c "echo 'export HADOOP_CONF_DIR=/opt/hadoop/conf' >> /etc/bash.bashrc"
sudo sh -c "echo 'export HADOOP_COMMON_HOME=/usr/lib/hadoop' >> /etc/bash.bashrc"
sudo sh -c "echo 'export HADOOP_HDFS_HOME=/usr/lib/hadoop-hdfs' >> /etc/bash.bashrc"
sudo sh -c "echo 'export HADOOP_MAPRED_HOME=/usr/lib/hadoop-mapreduce' >> /etc/bash.bashrc"
sudo sh -c "echo 'export HADOOP_YARN_HOME=/usr/lib/hadoop-yarn' >> /etc/bash.bashrc"
sudo sh -c "echo 'export HADOOP_LOG_DIR=/local_scratch/hadoop-hdfs/log' >> /etc/bash.bashrc"
sudo sh -c "echo 'export YARN_LOG_DIR=/local_scratch/hadoop-yarn/log' >> /etc/bash.bashrc"
sudo sh -c "echo 'export HADOOP_CMD=/usr/bin/hadoop' >> /etc/bash.bashrc"
sudo sh -c "echo 'export HADOOP_STREAMING=/usr/lib/hadoop-mapreduce/hadoop-streaming-2.6.0-cdh5.4.2.jar' >> /etc/bash.bashrc"

# set up path
sudo sh -c "echo 'export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$HADOOP_COMMON_HOME:\$HADOOP_COMMON_HOME/lib:\$HADOOP_HDFS_HOME:\$HADOOP_HDFS_HOME/lib:\$HADOOP_MAPRED_HOME:\$HADOOP_MAPRED_HOME/lib:\$HADOOP_YARN_HOME:\$HADOOP_YARN_HOME/lib:\$JAVA_HOME/jre/lib/amd64:\$JAVA_HOME/jre/lib/amd64/server' >> /etc/bash.bashrc"

sudo sh -c "echo 'export JAVA_LIBRARY_PATH=\$JAVA_LIBRARY_PATH:\$HADOOP_COMMON_HOME:\$HADOOP_COMMON_HOME/lib:\$HADOOP_HDFS_HOME:\$HADOOP_HDFS_HOME/lib:\$HADOOP_MAPRED_HOME:\$HADOOP_MAPRED_HOME/lib:\$HADOOP_YARN_HOME:\$HADOOP_YARN_HOME/lib' >> /etc/bash.bashrc"

sudo sh -c "echo 'export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig' >> /etc/bash.bashrc"

echo "Set up RAM disk to store stats"
echo "sudo mkdir /mnt/ramdisk"
sudo mkdir /mnt/ramdisk
echo "sudo mount -t tmpfs -o size=6g tmpfs /mnt/ramdisk"
sudo mount -t tmpfs -o size=6g tmpfs /mnt/ramdisk






