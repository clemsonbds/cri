#!/bin/bash

# set up system libraries
sudo apt-get -y install libcurl4-openssl-dev 

sudo git clone https://clemsonbds@bitbucket.org/clemsonbds/packages.git

# set up libraries for Thrift
sudo apt-get -y install libboost-dev libboost-test-dev libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libevent-dev automake libtool flex bison pkg-config g++ libssl-dev python-dev

echo "Installing thrift (commented out since not using rhbase) =========================================="
#cd /packages/thrift
#sudo tar xzf thrift-0.9.1.tar.gz 
#cd thrift-0.9.1/ 
#sudo ./configure 
#sudo make 
#sudo make install 
#cd /

echo "Installing gfortran ======================================================================"
sudo apt-get -y install gfortran

# set up R
echo "Installing R 3.2.0 ======================================================================="
sudo dpkg -i /packages/R/r-base-core_3.2.0-1trusty_amd64.deb
sudo apt-get -y -f install

echo "Installing R studio server on client node 192.168.1.1 ===================================="
myaddress=`ifconfig | grep 192.168`
echo $myaddress
# Client configuration
if [[ "$myaddress" == *"192.168.1.1 "* ]]; then
  echo "${field[0]} is client node"
  echo "sudo apt-get -y install gdebi-core"
  echo "##########################"
  sudo apt-get -y install gdebi-core
  sudo gdebi -n /packages/rstudio/rstudio-server-0.99.442-amd64.deb
  cd /home/hdfs
  sudo -u hdfs git clone https://clemsonbds@bitbucket.org/clemsonbds/rhadoopconnectors.git
fi

sudo sh -c "echo 'HADOOP_CMD=/usr/bin/hadoop' >> /usr/lib/R/etc/Renviron.site"
sudo sh -c "echo 'HADOOP_STREAMING=/usr/lib/hadoop-mapreduce/hadoop-streaming-2.6.0-cdh5.4.2.jar' >> /usr/lib/R/etc/Renviron.site"
sudo sh -c "echo 'R_HOME=/usr/lib/R' >> /usr/lib/R/etc/Renviron.site"

echo "Have to install rjava from apt-get ======================================================="
sudo apt-get -y install r-cran-rjava

# set up R packages from command line
cd /packages/R
sudo /cri/cloudlab/cloudera/packages.R


echo "H2O ======================================================================================"
sudo -u hdfs cp /packages/h2o/h2o-3.0.0.12-cdh5.3.zip /home/hdfs/
sudo -u hdfs unzip /home/hdfs/h2o-3.0.0.12-cdh5.3.zip -d /home/hdfs/



