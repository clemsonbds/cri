#!/bin/bash

# use node 1, 3, 5 as zookeeper nodes, node 1,2 are producers, 3,4 are brokers, 5,6 are consumers.
#Reference------------------------------------
#http://www.objectpartners.com/2014/05/06/setting-up-your-own-apache-kafka-cluster-with-vagrant-step-by-step/
#http://czcodezone.blogspot.com/2014/11/setup-kafka-in-cluster.html
#http://czcodezone.blogspot.sg/2014/11/setup-zookeeper-in-cluster.html
#http://czcodezone.blogspot.sg/2014/11/setup-zookeeper-on-single-machine.html
#kafka console api https://kafka.apache.org/082/quickstart.html

# set up system libraries
sudo apt-get -y install libcurl4-openssl-dev 

#sudo git clone https://clemsonbds@bitbucket.org/clemsonbds/packages.git

#wait for 5min
#sleep 5m

# set up passwordless ssh
sudo cp /cri/cloudlab/keys/* /users/yuhengdu/.ssh/
#sudo sh -c "echo `more /users/yuhengdu/.ssh/id_rsa.pub` >> /users/yuhengdu/.ssh/authorized_keys"
sudo chmod 600 /users/yuhengdu/.ssh/id_rsa
sudo chmod 644 /users/yuhengdu/.ssh/id_rsa.pub
sudo chown yuhengdu:bdslab-PG0 /users/yuhengdu/.ssh/id_rsa
sudo chown yuhengdu:bdslab-PG0 /users/yuhengdu/.ssh/id_rsa.pub

#set up zookeeper
echo "Configuring zookeeper ===================================================================="
cd /packages/zookeeper-3.4.6
sudo cp conf/zoo_sample.cfg conf/zoo.cfg
sudo chmod 755 conf/zoo.cfg
sudo mkdir -p /packages/zookeeper-3.4.6/data
sudo sed -i 's/dataDir=.*$/dataDir=\/packages\/zookeeper-3.4.6\/data/g' conf/zoo.cfg
echo "Setting myid ============================================================================="
myaddress=`ifconfig | grep 192.168`
echo $myaddress
re="inet (.*)192.168.1.([^[:space:]]+)( )+Bcast"
if [[ $myaddress =~ $re ]]; then
	myid=${BASH_REMATCH[2]}
	echo "my id is $myid"
fi
#setting up my id
sudo chmod -R 777 data
cd data 
if [ ! -f myid ]; then
	echo "myid file does not exist"
	sudo touch myid
	sudo chmod 777 myid
	sudo sh -c "echo $myid >> myid"
else
	echo "myid file already exists"
	sudo chmod 777 myid
	sudo sed -i "1s/.*/$myid/g" myid
fi
cd /packages/zookeeper-3.4.6
sudo sh -c "echo 'server.1=192.168.1.1:2888:3888' >> conf/zoo.cfg"
sudo sh -c "echo 'server.3=192.168.1.3:2888:3888' >> conf/zoo.cfg"
sudo sh -c "echo 'server.5=192.168.1.5:2888:3888' >> conf/zoo.cfg"
sudo chmod 755 bin/zkServer.sh
#start zookeeper
echo "Staring zookeeper ========================================================================"
sudo sh -c "bin/zkServer.sh start" 
#check zookeeper status
echo "Check zookeeper status ==================================================================="
sudo sh -c "bin/zkServer.sh status"
#set up gradle
#set path for gradle
#config kafka
cd /packages/kafka-0.8.2.1-src
sudo chmod 755 config/server.properties
sudo sed -i "s/broker.id=.*$/broker.id=$myid/g" config/server.properties
sudo sh -c "echo 'hostname=192.168.1.$myid' >> config/server.properties"
sudo sed -i 's/zookeeper.connect=.*$/zookeeper.connect=192.168.1.1:2181,192.168.1.3:2181,192.168.1.5:2181/g' config/server.properties 
#start kafka service
sudo sh -c "bin/kafka-server-start.sh config/server.properties"
