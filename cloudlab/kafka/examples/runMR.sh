#!/bin/bash
classname=$1
sourcefile="$classname.java"
jarfile="$classname.jar"

rm -Rf classes
rm $jarfile
mkdir classes

echo "Compiling ..."
javac -cp $HADOOP_COMMON_HOME/hadoop-common-2.5.0-cdh5.3.3.jar:$HADOOP_MAPRED_HOME/hadoop-mapreduce-client-core-2.5.0-cdh5.3.3.jar:$HADOOP_MAPRED_HOME/lib/log4j-1.2.17.jar:. -d classes $sourcefile

echo "Creating jar ..."
jar -cvf $jarfile -C classes/ .

