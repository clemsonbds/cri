#!/bin/bash

# set up passwordless ssh
sudo cp /cri/cloudlab/keys/* /users/lngo/.ssh/
sudo sh -c "echo `more /users/lngo/.ssh/id_rsa.pub` >> /users/lngo/.ssh/authorized_keys"
sudo chmod 600 /users/lngo/.ssh/id_rsa
sudo chmod 644 /users/lngo/.ssh/id_rsa.pub
sudo chown lngo:bdslab-PG0 /users/lngo/.ssh/id_rsa
sudo chown lngo:bdslab-PG0 /users/lngo/.ssh/id_rsa.pub

# set up local_scratch to be owned by the username
sudo chown -R lngo:bdslab-PG0 /local

sudo groupadd hadoop
sudo yes "" | sudo adduser --ingroup admin --disabled-password hdfs
sudo yes "" | sudo adduser --ingroup admin --disabled-password mapred
sudo yes "" | sudo adduser --ingroup admin --disabled-password yarn

sudo groupadd hdfs
sudo groupadd mapred
sudo groupadd yarn


sudo usermod -aG hadoop hdfs
sudo usermod -aG hdfs hdfs
sudo usermod -aG hadoop mapred
sudo usermod -aG mapred mapred
sudo usermod -aG hadoop yarn
sudo usermod -aG yarn yarn
sudo usermod -aG hadoop lngo

# set up passwordless ssh
sudo su hdfs -c "mkdir -p /home/hdfs/.ssh/"
sudo su hdfs -c "cp /cri/cloudlab/keys/* /home/hdfs/.ssh/"
sudo su hdfs -c "cp /home/hdfs/.ssh/id_rsa.pub /home/hdfs/.ssh/authorized_keys"
sudo chmod 600 /home/hdfs/.ssh/id_rsa
sudo chmod 644 /home/hdfs/.ssh/id_rsa.pub
sudo chown hdfs:admin /home/hdfs/.ssh/id_rsa
sudo chown hdfs:admin /home/hdfs/.ssh/id_rsa.pub

# setup Hadoop configuration files
sudo mkdir -p /opt/hadoop/conf/
sudo cp -R /cri/cloudlab/cloudera/conf/* /opt/hadoop/conf/
source /etc/bash.bashrc

HADOOP_NAMENODE=`sudo su lngo -c "ssh -oStrictHostKeyChecking=no lngo@192.168.1.2 'hostname -f'"`
sudo sed -i 's:HADOOP_NAMENODE:'"$HADOOP_NAMENODE"':g' /opt/hadoop/conf/core-site.xml
RESOURCE_NODE=`sudo su lngo -c "ssh -oStrictHostKeyChecking=no lngo@192.168.1.3 'hostname -f'"`
sudo sed -i 's:RESOURCE_NODE:'"$RESOURCE_NODE"':g' /opt/hadoop/conf/yarn-site.xml
HISTORY_SERVER=`sudo su lngo -c "ssh -oStrictHostKeyChecking=no lngo@192.168.1.4 'hostname -f'"`
sudo sed -i 's:HISTORY_SERVER:'"$HISTORY_SERVER"':g' /opt/hadoop/conf/mapred-site.xml

#HADOOP_NAMENODE=`sudo su lngo -c "ssh -oStrictHostKeyChecking=no lngo@192.168.1.2 'hostname'"`
#sudo sed -i 's:HADOOP_NAMENODE:'"$HADOOP_NAMENODE"':g' /opt/hadoop/conf/core-site.xml
#RESOURCE_NODE=`sudo su lngo -c "ssh -oStrictHostKeyChecking=no lngo@192.168.1.3 'hostname'"`
#sudo sed -i 's:RESOURCE_NODE:'"$RESOURCE_NODE"':g' /opt/hadoop/conf/yarn-site.xml
#HISTORY_SERVER=`sudo su lngo -c "ssh -oStrictHostKeyChecking=no lngo@192.168.1.4 'hostname'"`
#sudo sed -i 's:HISTORY_SERVER:'"$HISTORY_SERVER"':g' /opt/hadoop/conf/mapred-site.xml

sudo mkdir -p /local_scratch/hadoop-hdfs
sudo mkdir -p /local_scratch/hadoop-hdfs/namenode
sudo mkdir -p /local_scratch/hadoop-hdfs/datanode
sudo mkdir -p /local_scratch/hadoop-hdfs/log
sudo mkdir -p /local_scratch/hadoop-hdfs/tmp
echo "sudo chown -R hdfs:hdfs /local_scratch/hadoop-hdfs/"
sudo chown -R hdfs:hdfs /local_scratch/hadoop-hdfs/
sudo sh -c "echo 'export HADOOP_LOG_DIR=/local_scratch/hadoop-hdfs/log' >> /opt/hadoop/conf/hadoop-env.sh"
sudo mkdir -p /local_scratch/hadoop-yarn/data
sudo mkdir -p /local_scratch/hadoop-yarn/log
echo "sudo chown -R yarn:yarn /local_scratch/hadoop-yarn/"
sudo chown -R yarn:yarn /local_scratch/hadoop-yarn/ 
sudo sh -c "echo 'export YARN_LOG_DIR=/local_scratch/hadoop-yarn/log' >> /opt/hadoop/conf/yarn-env.sh"

# set up software
echo "INSTALL HADOOP SOFTWARE #####################################"
myaddress=`ifconfig | grep 192.168`
echo $myaddress
# Client configuration
if [[ "$myaddress" == *"192.168.1.1 "* ]]; then
  echo "${field[0]} is name node"
  echo "sudo apt-get -y install hadoop-client"
  echo "##########################"
  sudo apt-get -y install hadoop-client
  sudo -u hdfs cp /cri/cloudlab/cloudera/examples/* /home/hdfs/
elif [[ "$myaddress" == *"192.168.1.2 "* ]]; then
  echo "${field[0]} is name node"
  echo "sudo apt-get -y install hadoop-hdfs-namenode"
  echo "##########################"
  sudo apt-get -y install hadoop-hdfs-namenode
  sudo update-alternatives --install /etc/hadoop/conf hadoop-conf /opt/hadoop/conf 50
  sudo update-alternatives --set hadoop-conf /opt/hadoop/conf
  sudo -u hdfs hdfs namenode -format -force
  sudo service hadoop-hdfs-namenode stop
  sudo service hadoop-hdfs-namenode start
  sleep 2m
  # set up directories for yarn
  sudo -u hdfs hadoop fs -mkdir /tmp
  sudo -u hdfs hadoop fs -chmod -R 1777 /tmp

  sudo -u hdfs hadoop fs -mkdir /user
  sudo -u hdfs hadoop fs -mkdir /user/history
  sudo -u hdfs hadoop fs -chmod -R 1777 /user/history
  sudo -u hdfs hadoop fs -chown mapred:hadoop /user/history

  sudo -u hdfs hadoop fs -mkdir /var
  sudo -u hdfs hadoop fs -mkdir /var/log
  sudo -u hdfs hadoop fs -mkdir /var/log/hadoop-yarn
  sudo -u hdfs hadoop fs -chown yarn:mapred /var/log/hadoop-yarn

  sudo -u hdfs hadoop fs -copyFromLocal /cri/cloudlab/cloudera/examples/complete-shakespeare.txt /complete.txt
# Resource manager configuration
elif [[ "$myaddress" == *"192.168.1.3 "* ]]; then
  echo "${field[0]} is resource manager"
  echo "sudo apt-get -y install hadoop-yarn-resourcemanager"
  echo "##########################"
  sudo apt-get -y install hadoop-yarn-resourcemanager
  sudo update-alternatives --install /etc/hadoop/conf hadoop-conf /opt/hadoop/conf 50
  sudo update-alternatives --set hadoop-conf /opt/hadoop/conf
  sudo service hadoop-yarn-resourcemanager stop
  sudo service hadoop-yarn-resourcemanager start
# History servers configuration
elif [[ "$myaddress" == *"192.168.1.4 "* ]]; then
  echo "${field[0]} is history node"
  echo "sudo apt-get -y install hadoop-mapreduce-historyserver hadoop-yarn-proxyserver"
  echo "##########################"
  sleep 5m
#  sudo apt-get -y install hadoop-mapreduce-historyserver hadoop-yarn-proxyserver
  sudo apt-get -y install hadoop-mapreduce-historyserver
  sudo update-alternatives --install /etc/hadoop/conf hadoop-conf /opt/hadoop/conf 50
  sudo update-alternatives --set hadoop-conf /opt/hadoop/conf
  sleep 5m
  sudo service hadoop-mapreduce-historyserver stop
  sudo service hadoop-mapreduce-historyserver start
# Datanode/Nodemanager configuration
else
  echo "${field[0]} is data node"
  echo "sudo apt-get -y install hadoop-yarn-nodemanager hadoop-hdfs-datanode hadoop-mapreduce"
  echo "##########################"
  sudo apt-get -y install hadoop-yarn-nodemanager hadoop-hdfs-datanode hadoop-mapreduce
  sudo update-alternatives --install /etc/hadoop/conf hadoop-conf /opt/hadoop/conf 50
  sudo update-alternatives --set hadoop-conf /opt/hadoop/conf
  sudo service hadoop-hdfs-datanode stop
  sudo service hadoop-hdfs-datanode start
  sudo service hadoop-yarn-nodemanager stop
  sudo service hadoop-yarn-nodemanager start
fi
echo "##############################################################"








