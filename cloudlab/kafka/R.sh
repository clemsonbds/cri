#!/bin/bash

# set up system libraries
sudo apt-get -y install libcurl4-openssl-dev 

sudo git clone https://clemsonbds@bitbucket.org/clemsonbds/packages.git

# set up libraries for Thrift
sudo apt-get -y install libboost-dev libboost-test-dev libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libevent-dev automake libtool flex bison pkg-config g++ libssl-dev python-dev

echo "Installing thrift (commented out since not using rhbase) =========================================="
#cd /packages/thrift
#sudo tar xzf thrift-0.9.1.tar.gz 
#cd thrift-0.9.1/ 
#sudo ./configure 
#sudo make 
#sudo make install 
#cd /

echo "Installing gfortran ======================================================================"
sudo apt-get -y install gfortran

# set up R
echo "Installing R 3.2.0 ======================================================================="
sudo dpkg -i /packages/R/r-base-core_3.2.0-1trusty_amd64.deb
sudo apt-get -y -f install

echo "Have to install rjava from apt-get ======================================================="
sudo apt-get -y install r-cran-rjava

# set up R packages from command line
cd /packages/R
sudo /cri/cloudlab/cloudera/packages.R


echo "H2O ======================================================================================"
sudo -u hdfs cp /packages/h2o/h20-3.0.0.12-cdh5.3.zip /home/hdfs/


