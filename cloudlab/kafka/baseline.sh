#!/bin/bash
sudo apt-get -y update


#sudo wget http://archive.cloudera.com/cdh5/ubuntu/trusty/amd64/cdh/cloudera.list -O /etc/apt/sources.list.d/cloudera.list
sudo sh -c "echo 'deb [arch=amd64] http://archive.cloudera.com/cdh5/ubuntu/trusty/amd64/cdh trusty-cdh5.3.3 contrib' >> /etc/apt/sources.list.d/cloudera.list"
sudo sh -c "echo 'deb-src http://archive.cloudera.com/cdh5/ubuntu/precise/amd64/cdh trusty-cdh5.3.3 contrib' >> /etc/apt/sources.list.d/cloudera.list"
sudo cp /cri/cloudlab/cloudera/preferences.d/cloudera.pref /etc/apt/preferences.d/cloudera.pref
sudo apt-get update

# set up JRE
sudo apt-get install openjdk-7-jre

# set up Java
sudo apt-get -y install openjdk-7-jdk
sudo sh -c "echo 'export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64' >> /etc/bash.bashrc"
sudo sh -c "echo 'export PATH=\$PATH:\$JAVA_HOME/bin' >> /etc/bash.bashrc"

