#!/bin/bash
# set up passwordless ssh
sudo cp /cri/cloudlab/keys/* /users/lngo/.ssh/
sudo sh -c "echo `more /users/lngo/.ssh/id_rsa.pub` >> /users/lngo/.ssh/authorized_keys"
sudo chmod 600 /users/lngo/.ssh/id_rsa
sudo chmod 644 /users/lngo/.ssh/id_rsa.pub
sudo chown lngo:bdslab-PG0 /users/lngo/.ssh/id_rsa
sudo chown lngo:bdslab-PG0 /users/lngo/.ssh/id_rsa.pub

sudo cp /cri/cloudlab/default_conf/modules /etc/modules

sudo yes "" | sudo adduser --ingroup admin --disabled-password hpcc

sudo apt-get update

sudo wget http://cdn.hpccsystems.com/releases/CE-Candidate-5.2.2/bin/platform/hpccsystems-platform-community_5.2.2-1trusty_amd64.deb

sudo dpkg -i hpccsystems-platform-community_5.2.2-1trusty_amd64.deb

sudo apt-get -y -f install

sudo apt-get -y install lm-sensors

sudo yes "" | sudo sensors-detect

sudo modprobe ipmi_msghandler

sudo modprobe ipmi_devintf

sudo modprobe ipmi_si

sudo modprobe i2c-dev

sudo modprobe i2c-i801

sudo modprobe cpuid

sudo apt-get -y install ipmitool

sudo apt-get -y install module-assistant

sudo module-assistant update

