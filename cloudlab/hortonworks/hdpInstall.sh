#!/bin/bash

#sudo cp /cri/cloudlab/default_conf/modules /etc/modules
#sudo yes "" | sudo adduser --ingroup admin --disabled-password hpcc

sudo yum -y install OpenIPMI OpenIPMI-libs OpenIPMI-perl OpenIPMI-python

sudo modprobe ipmi_msghandler

sudo modprobe ipmi_devintf

sudo modprobe ipmi_si

sudo modprobe i2c-dev

sudo modprobe i2c-i801

sudo modprobe cpuid

sudo yum install lm_sensors.x86_64

sudo yes "" | sudo sensors-detect

#
#
# Set up basis system requrements for hortonworks (without support for Hive and Oozie for now (testing PigMix only)
#
#

# set up basic software packages
sudo yum -y update
sudo yum -y install epel-release
sudo yum -y install pdsh

# setup repository for Hortonworks
sudo wget -nv http://public-repo-1.hortonworks.com/HDP/centos5/1.x/GA/1.3.0.0/hdp.repo  -O /etc/yum.repos.d/hdp.repo
sudo wget -nv http://public-repo-1.hortonworks.com/ambari/centos5/1.x/updates/1.2.4.9/ambari.repo -O /etc/yum.repos.d/ambari.repo

# set up Java
sudo yum -y install java-1.7.0-openjdk-devel
sudo sh -c "echo 'export JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.79.x86_64' >> /etc/profile"
sudo sh -c "echo 'export PATH=$PATH:$JAVA_HOME/bin' >> /etc/profile"

# add git to path
sudo sh -c "echo 'export PATH=$PATH:/usr/local/git/bin' >> /etc/profile"


